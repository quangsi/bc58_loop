/**
 * 
var count = 0;

while (count < 3) {
  count++;
  console.log("Hello", count);
}

 */

var count = 0;

do {
  count++;
  console.log("Hello", count);
} while (count < 3);

// làm trước r tính sau

// chọn nhân vật

// do {
// chọn trang bị cho nhân vật
// } while ("khi user set up sai  ");

// vào game

// 2==2
// true==true
// flase == false

var number = 20;
// input 5 => 1 + 2 +3 +4 +5  => 15
var count = 0;
var sum = 0;
do {
  count++;
  sum = sum + count;
} while (count < number);
console.log("😀 - number", sum); //231
